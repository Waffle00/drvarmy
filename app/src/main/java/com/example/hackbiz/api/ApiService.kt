package com.example.hackbiz.api

import com.example.hackbiz.base.BaseResponse
import com.example.hackbiz.data.request.ProfileRequest
import com.example.hackbiz.data.response.ProfileResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST


interface ApiService {
    @POST("auth/me")
    suspend fun getProfile(
        @Body profileRequest: ProfileRequest
    ) : BaseCallback<ProfileResponse>
}

typealias BaseCallback<T> = Response<BaseResponse<T>>
