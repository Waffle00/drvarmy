package com.example.onboardingfix.data

import com.example.hackbiz.base.BaseRemote
import com.example.hackbiz.ResourceState
import com.example.hackbiz.ResponseWrapper
import com.example.hackbiz.data.AppDataBase
import com.example.hackbiz.data.entity.*

class LocalDataSource(private val appDataBase : AppDataBase) : BaseRemote(){
    private suspend fun <T> getResult(request: suspend () -> T): ResourceState<ResponseWrapper<T>> {
        return try {
            val res = request.invoke()
            return ResourceState.Success(ResponseWrapper(null, data = res, errorResponse = null))
        } catch (e: Exception) {
            errorState(msg = e.toString())
        }
    }

    suspend fun deleteAllTables() = getResult {
        appDataBase.clearAllTables()
    }

    suspend fun insertProfile(profile: ProfileEntity) = getResult {
        appDataBase.profileDao().insertProfile(profile)
    }

    suspend fun getProfileLocal() = getResult {
        appDataBase.profileDao().getProfileLocal()
    }

    suspend fun deleteTableProfile() = getResult {
        appDataBase.profileDao().deleteProfile()
    }
}