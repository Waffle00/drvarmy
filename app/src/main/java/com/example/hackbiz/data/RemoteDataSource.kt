package com.example.onboardingfix.data

import com.example.hackbiz.api.ApiService
import com.example.hackbiz.ResourceState
import com.example.hackbiz.ResponseWrapper
import com.example.hackbiz.base.BaseRemote
import com.example.hackbiz.base.BaseResponse
import com.example.hackbiz.data.request.ProfileRequest
import retrofit2.Response
import java.net.UnknownHostException

class RemoteDataSource(private val networkService: ApiService) : BaseRemote() {
    private suspend fun <T> getValue(request: suspend () -> Response<BaseResponse<T>>): ResourceState<ResponseWrapper<T>> {
        return try {
            val response = request()
            val body = response.body()

            if (response.isSuccessful.not() || body == null) {
                return errorState(response.code(), response.message())
            }

            return ResourceState.Success(
                ResponseWrapper(
                    body?.message ?: "",
                    body?.data,
                    null
                )
            )

        } catch (e: Exception) {
            errorState(msg = if (e is UnknownHostException) NO_INTERNET else e.localizedMessage.orEmpty())
        }
    }

    suspend fun getProfile(profileRequest: ProfileRequest) = suspendDataResult {
        getValue {
            networkService.getProfile(profileRequest)
        }
    }

    companion object {
        private const val NO_INTERNET = "Tidak ada koneksi internet"
    }
}