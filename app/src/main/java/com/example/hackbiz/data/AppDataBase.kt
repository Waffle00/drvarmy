package com.example.hackbiz.data

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.hackbiz.data.Dao.*
import com.example.hackbiz.data.entity.*

@Database(
    entities = [
        ProfileEntity::class
    ],
    version = 6,
    exportSchema = false
)
abstract class AppDataBase : RoomDatabase() {
    abstract fun profileDao(): ProfileDao
}