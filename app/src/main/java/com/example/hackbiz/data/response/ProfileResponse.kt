package com.example.hackbiz.data.response

import com.google.gson.annotations.SerializedName

class ProfileResponse(
    @SerializedName("id")
    val id: Int,
    @SerializedName("full_name")
    val full_name: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("phone")
    val phone: String,
    @SerializedName("role")
    val role: Int
)