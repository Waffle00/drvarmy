package com.example.hackbiz.data

import com.example.hackbiz.ResourceState
import com.example.hackbiz.ResponseWrapper
import com.example.hackbiz.data.entity.ProfileEntity
import com.example.hackbiz.data.request.ProfileRequest
import com.example.hackbiz.data.response.ProfileResponse
import com.example.onboardingfix.data.LocalDataSource
import com.example.onboardingfix.data.RemoteDataSource

class AppRepository(
    private val remoteDataSource: RemoteDataSource,
    private val localDataSource: LocalDataSource
) {
    suspend fun clearAllTable() = with(localDataSource) {
        deleteAllTables()
        deleteTableProfile()
    }

    suspend fun getProfile(profileRequest: ProfileRequest) : ResourceState<ResponseWrapper<ProfileResponse>> {
        return remoteDataSource.getProfile(profileRequest)
    }

    suspend fun insertProfile(profile: ProfileEntity) = localDataSource.insertProfile(profile)

    suspend fun getProfileLocal() : ResourceState<ResponseWrapper<ProfileEntity>> {
        return localDataSource.getProfileLocal()
    }
}