package com.example.hackbiz.data.Dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.hackbiz.data.entity.ProfileEntity

@Dao
interface ProfileDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertProfile(profile : ProfileEntity)

    @Query("SELECT * FROM profile_db")
    suspend fun getProfileLocal() : ProfileEntity

    @Query("DELETE FROM profile_db")
    suspend fun deleteProfile()
}