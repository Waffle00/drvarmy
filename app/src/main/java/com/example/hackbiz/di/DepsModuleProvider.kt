package com.example.onboardingfix.di

import com.example.onboardingfix.modules.repository.RepositoryModule
import com.example.onboardingfix.modules.retrofit.RetrofitModule
import com.example.onboardingfix.modules.room.RoomModule
import com.example.hackbiz.modules.viewmodel.ViewModelModule
import org.koin.core.module.Module
import com.example.hackbiz.modules.common.CommonModule

object DepsModuleProvider {
    val modules: List<Module>
        get() {
            return ArrayList<Module>().apply {
                addAll(CommonModule.modules)
                addAll(ViewModelModule.modules)
                addAll(RepositoryModule.modules)
                addAll(RetrofitModule.modules)
                addAll(RoomModule.modules)
            }
        }
}