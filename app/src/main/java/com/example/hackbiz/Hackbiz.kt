package com.example.hackbiz

import android.app.Application
import com.airbnb.lottie.BuildConfig
import com.example.onboardingfix.di.DepsModuleProvider
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import timber.log.Timber

class Hackbiz : Application() {
    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG)
            Timber.plant(Timber.DebugTree())
        startKoin {
            androidLogger()
            androidContext(this@Hackbiz)
            modules(DepsModuleProvider.modules)
        }
    }
}