package com.example.hackbiz.base

import org.koin.core.module.Module

interface BaseModule {
    val modules: List<Module>
}
