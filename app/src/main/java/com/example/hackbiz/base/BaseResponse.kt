package com.example.hackbiz.base

import com.google.gson.annotations.SerializedName

data class BaseResponse<T>(
    @SerializedName("statusCode")
    val status : Int,
    @SerializedName("message")
    val message : String,
    @SerializedName("data")
    val `data`: T?
)
