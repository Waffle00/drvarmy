package com.example.hackbiz.base

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Bundle
import android.view.View
import androidx.annotation.DrawableRes
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.example.hackbiz.R
import com.example.hackbiz.utils.DiffCallback
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import org.koin.android.ext.android.inject
import timber.log.Timber

abstract class BaseFragment : Fragment() {

    protected val diffCallback: DiffCallback by inject()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        hideToolbar()
        onViewReady(savedInstanceState)
        observeData()
    }

    protected abstract fun onViewReady(savedInstanceState: Bundle?)

    protected abstract fun observeData()


    protected fun <T> LiveData<T>.onResult(action: (T) -> Unit) {
        observe(this@BaseFragment, Observer { data -> data?.let(action) })
    }

    protected fun hideToolbar(isHide: Boolean = false) {
        (activity as AppCompatActivity).supportActionBar?.let {
            if (isHide) {
                it.hide()
            } else {
                it.show()
            }
        }
    }

    protected fun requestPermission(
        permissions: List<String>,
        permissionGranted: () -> Unit,
        permissionDenied: () -> Unit
    ) {
        Dexter.withActivity(requireActivity())
            .withPermissions(permissions)
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    if (report.areAllPermissionsGranted().not()) {
                        permissionDenied.invoke()
                        return
                    }

                    if (report.isAnyPermissionPermanentlyDenied) {
                        permissionDenied.invoke()
                        return
                    }

                    if (report.areAllPermissionsGranted()) {
                        permissionGranted.invoke()
                        return
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: List<PermissionRequest>,
                    token: PermissionToken
                ) {
                    token.continuePermissionRequest()
                }
            }).withErrorListener {
                Timber.d(it.name)
            }
            .onSameThread()
            .check()
    }

    fun isOnline(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
            if (connectivityManager != null) {
                val capabilities =
                    connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
                if (capabilities != null) {
                    when {
                        capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {
                            return true
                        }
                        capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {
                            return true
                        }
                        capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> {
                            return true
                        }
                    }
                }
            }
        } else {
            val activeNetworkInfo = connectivityManager.activeNetworkInfo
            if (activeNetworkInfo != null && activeNetworkInfo.isConnected) {
                return true
            }
        }
        return false
    }

    fun setupToolbarProperties(
        toolbarId: Toolbar,
        title: String,
        @DrawableRes drawable: Int = R.drawable.ic_back
    ) {
        (activity as AppCompatActivity).run {
            setSupportActionBar(toolbarId)
            supportActionBar?.let {
                toolbarId.title = title
                toolbarId.setTitleTextColor(
                    resources.getColor(R.color.TextColor)
                )
                toolbarId.setNavigationOnClickListener {
                    onBackPressed()
                }
                toolbarId.setBackgroundColor(resources.getColor(R.color.white))
                it.setDisplayHomeAsUpEnabled(true)
                it.setDisplayShowHomeEnabled(true)
                it.setHomeAsUpIndicator(drawable)
            }
        }
    }
}