package com.example.hackbiz

sealed class ResourceState<out T> {
    data class Success<out T>(val result: T) : ResourceState<T>()
    data class Error<T>(
        val error: T
    ) : ResourceState<T>()
}

data class ErrorResponse(val code: Int? = 0, val msg: String? = "")
data class ResponseWrapper<out T>(
    val message: String?,
    val data: T?,
    val errorResponse: ErrorResponse?
)