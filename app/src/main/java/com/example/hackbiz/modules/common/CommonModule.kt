package com.example.hackbiz.modules.common

import com.example.hackbiz.base.BaseModule
import com.example.hackbiz.utils.DiffCallback
import org.koin.core.module.Module
import org.koin.dsl.module

object CommonModule : BaseModule {
    override val modules: List<Module>
        get() = listOf(commonModule)

    private val commonModule = module {
        single { provideDiffCallback() }
    }

    private fun provideDiffCallback() = DiffCallback()
}