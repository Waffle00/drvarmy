package com.example.onboardingfix.modules.retrofit

import com.example.hackbiz.api.ApiService
import com.example.hackbiz.base.BaseModule
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.module.Module
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object RetrofitModule : BaseModule {
    override val modules: List<Module>
        get() = listOf(
            retrofitModule,
            webServiceModule
        )

    private val BASE_URL = "http://unity1.zen-nith.my.id:9005/"
    private val webServiceModule = module {
        single { get<Retrofit>().create(ApiService::class.java) }
    }

    private val retrofitModule = module {
        single { provideOkHttpClient() }
        single { provideRetrofit(get()) }
    }

    private fun provideOkHttpClient(): OkHttpClient {
        val logging = HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }

        return OkHttpClient.Builder()
            .addInterceptor(logging)
            .connectTimeout(30L, TimeUnit.SECONDS)
            .writeTimeout(30L, TimeUnit.SECONDS)
            .readTimeout(30L, TimeUnit.SECONDS)
            .build()
    }

    private fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
        val gson = GsonBuilder()
            .setLenient()
            .create()
        return Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }


}