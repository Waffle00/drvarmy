package com.example.onboardingfix.modules.room

import androidx.room.Room
import com.example.hackbiz.data.AppDataBase
import com.example.onboardingfix.modules.room.RoomMigration.MIGRATION_1_2
import com.example.hackbiz.base.BaseModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.module.Module
import org.koin.dsl.module

object RoomModule : BaseModule {
    override val modules: List<Module>
        get() = listOf(roomModule)

    private val roomModule = module {
        single {
            Room.databaseBuilder(androidContext(), AppDataBase::class.java, "db")
                .addMigrations(
                    MIGRATION_1_2
                )
                .fallbackToDestructiveMigration()
                .build()
        }
    }
}