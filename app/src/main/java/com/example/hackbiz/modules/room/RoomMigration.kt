package com.example.onboardingfix.modules.room

import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase

object RoomMigration {
    val MIGRATION_1_2 = object : Migration(1, 2) {
        override fun migrate(database: SupportSQLiteDatabase) {
            database.execSQL(
                "CREATE TABLE IF NOT EXISTS class_db (id INTEGER PRIMARY KEY NOT NULL, name TEXT, code TEXT)"
            )
        }
    }
}